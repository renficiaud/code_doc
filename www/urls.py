from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include

from django.contrib import admin

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'', include('code_doc.urls')),
]

urlpatterns += static(settings.STATIC_URL,
                      document_root=settings.STATIC_ROOT)
