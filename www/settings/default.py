# flake8: noqa

"""
Django settings for www project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

SITE_NAME = 'CODEDOC'

BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir))

# path used to upload temporary files
USER_UPLOAD_TEMPORARY_STORAGE = os.path.join(BASE_DIR, 'temporary')
FILE_LOGGING_LOCATION = os.path.join(USER_UPLOAD_TEMPORARY_STORAGE, "%s.log" % SITE_NAME)

# creating needed directories
if not os.path.exists(USER_UPLOAD_TEMPORARY_STORAGE):
    os.makedirs(USER_UPLOAD_TEMPORARY_STORAGE)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_nx*8lt4e9rkkqkbc+@l+w3k1rpe@)mpidyy%=8nyo%w259l-_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# to be edited when in production mode
ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

ADMINS = (('Raffi Enficiaud', 'raffi.enficiaud@free.fr'),)

# the ppl receiving notifications for broken links if BrokenLinkEmailsMiddleware is active
MANAGERS = (('Raffi Enficiaud', 'raffi.enficiaud@free.fr'),)

# Email address that error messages come from.
SERVER_EMAIL = 'root@localhost'

# Default email address to use for various automated correspondence from
# the site managers.
DEFAULT_FROM_EMAIL = 'webmaster@localhost'

# Subject-line prefix for email messages send with django.core.mail.mail_admins
# or ...mail_managers.  Make sure to include the trailing space.
EMAIL_SUBJECT_PREFIX = '[code] '

# Host for sending email.
EMAIL_HOST = 'localhost'

# Port for sending email.
EMAIL_PORT = 25

# Optional SMTP authentication information for EMAIL_HOST.
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'code_doc',
)

MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'code_doc.permissions.backend.CodedocPermissionBackend',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '*** [%(levelname)s] %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '[%(levelname)s] %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': FILE_LOGGING_LOCATION,
        },

        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'code_doc.views': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.admin': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.templatetags.button_add_with_permission': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.templatetags.markdown_filter': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.models': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.forms': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.permissions': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        },
        'code_doc.signals': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'code_doc.migrations': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

ROOT_URLCONF = 'www.urls'
WSGI_APPLICATION = 'www.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media') + '/'
MEDIA_URL = '/media/'

LOGIN_URL = "/accounts/login/"
LOGIN_REDIRECT_URL = "/"
