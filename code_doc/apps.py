from django.apps import AppConfig


class CodedocAppConfig(AppConfig):

    name = 'code_doc'
    verbose_name = 'Code Doc'

    def ready(self):
        import code_doc.signals.signal_handlers  # pylint: disable=unused-import # noqa: F401
        import code_doc.signals.project_handlers  # pylint: disable=unused-import # noqa: F401
