# -*- coding: utf-8 -*-
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('code_doc', '0008_auto_20150420_1404'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='django_user',
            field=models.OneToOneField(related_name='author', null=True, blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.SET_NULL),
        ),
    ]
