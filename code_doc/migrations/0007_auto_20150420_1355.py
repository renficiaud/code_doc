# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    atomic = False
    dependencies = [
        ('code_doc', '0006_auto_20150420_1353'),
    ]

    operations = [
        migrations.RenameModel('ProjectVersion', 'ProjectSeries')
    ]
