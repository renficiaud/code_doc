# this file tests the correct behaviour of the add buttons

from django.test import TestCase
from django.contrib.auth.models import User


class TemplateGravatarTest(TestCase):

    def setUp(self):
        # dummy setup
        self.first_user = User.objects.create_user(username='toto', password='titi',
                                                   first_name='toto', last_name='titi',
                                                   email='blah@blah.org')

    def test_gravatar_smoke_test(self):
        """Tests gravatar rendering"""
        from django.template import Context, Template

        context = Context({'user': self.first_user})
        rendered = Template(
            '{% load gravatar %}'
            '{% gravatar_url user.email %}'
        ).render(context)

        self.assertIn('www.gravatar.com', rendered)
