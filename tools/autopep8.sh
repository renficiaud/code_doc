#!/bin/bash

# If you want to use a particular command, you can just define AUTOFORMAT_PROGRAM env variable
: ${AUTOFORMAT_PROGRAM:=autopep8}


UPDIR=`dirname $0`/..

LOCATIONS=${@:-$UPDIR/}

the_command () {
    find $LOCATIONS -type f \( -name "*.py" \) -not -path "*/build*" -exec "${AUTOFORMAT_PROGRAM}" --global-config $UPDIR/tools/autopep8_config -i  {} +
}

the_command
